var fakeData = [
    {date: new Date ('2015-11-01'), week: 55, netComp: 682, analyzerHr: 470, questionRight: 22},
    {date: new Date ('2015-11-08'), week: 56, netComp: 743, analyzerHr: 435, questionRight: 138},
    {date: new Date ('2015-11-15'), week: 57, netComp: 877, analyzerHr: 301, questionRight: 126},
    {date: new Date ('2015-11-22'), week: 58, netComp: 866, analyzerHr: 452, questionRight: 128},
    {date: new Date ('2015-11-29'), week: 59, netComp: 976, analyzerHr: 471, questionRight: 91},
    {date: new Date ('2015-12-06'), week: 50, netComp: 770, analyzerHr: 596, questionRight: 255},
    {date: new Date ('2015-12-13'), week: 51, netComp: 726, analyzerHr: 434, questionRight: 198},
    {date: new Date ('2015-12-20'), week: 52, netComp: 678, analyzerHr: 449, questionRight: 97},
    {date: new Date ('2015-12-27'), week: 53, netComp: 302, analyzerHr: 221, questionRight: 60}
];
var chart = AmCharts.makeChart("chartdiv-first",
    {
    "type": "serial",
    "theme": "light",
    "legend": {
        "useGraphSettings": true
    },
    "valueAxes": [{
        "id":"v1",
        "axisColor": "#d8d8d8",
        "axisThickness": 2,
        "gridAlpha": 0,
        "axisAlpha": 1,
        "position": "left"
    }],
    "graphs": [{
        "valueAxis": "v1",
        "lineColor": "#ed6e37",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "netComp",
        "valueField": "netComp",
        "fillAlphas": 0
    }, {
        "valueAxis": "v1",
        "lineColor": "#259e01",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "analyzerHr",
        "valueField": "analyzerHr",
        "fillAlphas": 0
    }, {
        "valueAxis": "v1",
        "lineColor": "#15a0c8",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "questionRight",
        "valueField": "questionRight",
        "fillAlphas": 0
    }],
    "chartCursor": {
        "cursorPosition": "mouse"
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "axisColor": "#d8d8d8",
        "minorGridEnabled": true
    },
    "export": {
        "enabled": true,
        "position": "bottom-right"
    },
    "dataProvider": fakeData

    });

var chartSecond = AmCharts.makeChart("chartdiv-second", {
    "type": "serial",
    "theme": "light",
    "categoryField": "week",
    "startDuration": 1,
    "legend": {
        "useGraphSettings": true
    },
    "categoryAxis": {
        "gridPosition": "start",
        "position": "left"
    },
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "Net Comp :[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-1",
            "lineAlpha": 0.2,
            "lineColor": "#ed6e37",
            "title": "netComp",
            "type": "column",
            "valueField": "netComp"
        },
        {
            "balloonText": "Analyzer HR :[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-2",
            "lineAlpha": 0.2,
            "title": "Analyzer HR",
            "lineColor": "#259e01",
            "type": "column",
            "valueField": "analyzerHr"
        },
        {
            "balloonText": "Question Right :[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-3",
            "lineAlpha": 0.2,
            "title": "Question Right",
            "type": "column",
            "valueField": "questionRight",
            "lineColor": "#15a0c8"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "position": "bottom",
            "axisAlpha": 0
        }
    ],
    "allLabels": [],
    "balloon": {},
    "titles": [],
    "export": {
        "enabled": true
    },
    "dataProvider": fakeData
});

function alertFunction(){
    alert('do smt');
}